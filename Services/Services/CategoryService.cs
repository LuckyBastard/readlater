﻿using Data;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ReadLaterDataContext _ReadLaterDataContext;
        private readonly IUserService _userService;

        public CategoryService(ReadLaterDataContext readLaterDataContext, IUserService userService)
        {
            _ReadLaterDataContext = readLaterDataContext;
            _userService = userService;
        }

        // PUBLIC 

        public Category EnsureCategoryExists(Category category)
        {
            category.UserID = _userService.GetUserId();
            var existingCategory = _ReadLaterDataContext.Categories
                .FirstOrDefault(cat => cat.Name == category.Name && cat.UserID == category.UserID);

            if (existingCategory != null)
                return existingCategory;

            _ReadLaterDataContext.Add(category);
            _ReadLaterDataContext.SaveChanges();
            return category;
        }

        public void UpdateCategory(Category category)
        {
            _ReadLaterDataContext.Update(category);
            _ReadLaterDataContext.SaveChanges();
        }

        public List<Category> GetCategories()
        {
            return _ReadLaterDataContext.Categories.Where(c => c.UserID == _userService.GetUserId()).ToList();
        }

        public Category GetCategory(int Id)
        {
            return _ReadLaterDataContext.Categories.Where(c => c.ID == Id).FirstOrDefault();
        }

        public Category GetCategory(string Name)
        {
            return _ReadLaterDataContext.Categories.Where(c => c.Name == Name).FirstOrDefault();
        }

        public void DeleteCategory(Category category)
        {
            foreach (Bookmark bookmark in _ReadLaterDataContext.Bookmarks
                .Where(b => b.CategoryId.HasValue && (int)b.CategoryId == category.ID))
            {
                bookmark.CategoryId = null;
                bookmark.Category = null;
            }
            _ReadLaterDataContext.Categories.Remove(category);
            _ReadLaterDataContext.SaveChanges();
        }

        public IEnumerable<Bookmark> GetBookmarks(int categoryID)
        {
            return _ReadLaterDataContext.Bookmarks.Where(b => b.CategoryId == categoryID).AsEnumerable();
        }

    }
}
