﻿using Entity;
using System.Collections.Generic;

namespace Services
{
    public interface IBookmarkService
    {
        Bookmark CreateBookmark(Bookmark Bookmark);
        IEnumerable<Bookmark> GetBookmarks();
        IEnumerable<Bookmark> GetBookmarks(int categoryId);
        Bookmark GetBookmark(int Id);
        Bookmark GetBookmark(string ShortDescription);
        void UpdateBookmark(Bookmark Bookmark);
        void DeleteBookmark(Bookmark Bookmark);
    }
}
