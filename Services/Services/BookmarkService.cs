﻿using Data;
using Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class BookmarkService : IBookmarkService
    {
        private readonly ReadLaterDataContext _ReadLaterDataContext;
        private readonly IUserService _userService;
        private readonly ICategoryService _categoryService;

        public BookmarkService(ReadLaterDataContext readLaterDataContext, IUserService userService, ICategoryService categoryService) 
        {
            _ReadLaterDataContext = readLaterDataContext;
            _userService = userService;
            _categoryService = categoryService;
        }

        // PUBLIC

        public Bookmark CreateBookmark(Bookmark bookmark)
        {
            bookmark.CreateDate = DateTime.Now;
            bookmark.UserID = _userService.GetUserId();
            _ReadLaterDataContext.Add(bookmark);
            _ReadLaterDataContext.SaveChanges();
            return bookmark;
        }

        public void UpdateBookmark(Bookmark bookmark)
        {
            ResolveCategoryCreation(bookmark);
            _ReadLaterDataContext.Update(bookmark);
            _ReadLaterDataContext.SaveChanges();
        }

        public IEnumerable<Bookmark> GetBookmarks()
        {
            return _ReadLaterDataContext.Bookmarks
                .Include(b => b.Category)
                .Where(b => b.UserID == _userService.GetUserId())
                .AsEnumerable();
        }
        
        public IEnumerable<Bookmark> GetBookmarks(int categoryId)
        {
            return _ReadLaterDataContext.Bookmarks
                .Include(b => b.Category)
                .Where(b => b.CategoryId == categoryId && b.UserID == _userService.GetUserId())
                .AsEnumerable();
        }

        public Bookmark GetBookmark(int Id)
        {
            return _ReadLaterDataContext.Bookmarks
                .Include(b => b.Category)
                .Where(c => c.ID == Id).FirstOrDefault();
        }

        public Bookmark GetBookmark(string ShortDescription)
        {
            return _ReadLaterDataContext.Bookmarks
                .Where(c => c.ShortDescription == ShortDescription && c.UserID == _userService.GetUserId())
                .FirstOrDefault();
        }

        public void DeleteBookmark(Bookmark bookmark)
        {
            _ReadLaterDataContext.Bookmarks.Remove(bookmark);
            _ReadLaterDataContext.SaveChanges();
        }

        // PRIVATE

        private void ResolveCategoryCreation(Bookmark bookmark)
        {
            if (!string.IsNullOrWhiteSpace(bookmark.Category?.Name))
            {
                var existingCategory = _categoryService.GetCategory(bookmark.Category.Name);

                if (existingCategory != null)
                    bookmark.Category = existingCategory;
                else
                    _categoryService.EnsureCategoryExists(
                        new Category()
                        {
                            Name = bookmark.Category.Name
                        });
            }
            else // ensure new category isn't selected or created for this bookmark
                bookmark.Category = null;
        }

    }
}
