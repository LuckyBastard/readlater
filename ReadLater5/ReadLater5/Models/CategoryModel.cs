﻿using Entity;
using System.ComponentModel.DataAnnotations;

namespace ReadLater5.Models
{
    public class CategoryModel
    {
        [Required]
        public string Name { get; set; }

        public Category ToCategory()
        {
            return new Category()
            {
                Name = Name
            };
        }
    }
}
