﻿using Entity;
using System.Collections.Generic;

namespace ReadLater5.Models
{
    public class CategoryDeleteModel
    {
        public Category Category { get; set; }
        
        public IEnumerable<Bookmark> BookmarksToUncategorize { get; set; }
    }
}
