﻿using Entity;
using System;
using System.ComponentModel.DataAnnotations;

namespace ReadLater5.Models
{
    public class BookmarkModel
    {
        public int ID { get; set; }

        [Required]
        public string BookmarkURL { get; set; }
        
        [Required]
        public string BookmarkDescription { get; set; }
        
        public string CategoryName { get; set; }

        public DateTime CreateDate { get; set; }

        public string UserID { get; set; }

        public BookmarkModel()
        {

        }

        public BookmarkModel(Bookmark bookmark)
        {
            ID = bookmark.ID;
            BookmarkURL = bookmark.URL;
            BookmarkDescription = bookmark.ShortDescription;
            CategoryName = bookmark.Category?.Name;
            CreateDate = bookmark.CreateDate;
            UserID = bookmark.UserID;
        }

        public Bookmark ToBookmark()
        {
            return new Bookmark()
            {
                ID = ID,
                URL = BookmarkURL,
                ShortDescription = BookmarkDescription,
                CreateDate = CreateDate,
                UserID = UserID
            };
        }
    }
}
