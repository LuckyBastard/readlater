﻿using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReadLater5.Models;
using Services;
using System.Collections.Generic;
using System.Linq;

namespace ReadLater5.Controllers
{
    [Authorize]
    public class BookmarksController : Controller
    {
        private readonly IBookmarkService _bookmarkService;
        private readonly ICategoryService _categoryService;

        public BookmarksController(IBookmarkService bookmarkService, ICategoryService categoryService)
        {
            _bookmarkService = bookmarkService;
            _categoryService = categoryService;
        }

        [Authorize]
        public IActionResult Index()
        {
            var model = _bookmarkService.GetBookmarks();
            return View(model);
        }

        [Authorize]
        public IActionResult Details(int? id)
        {
            if (id == null)
                return new StatusCodeResult(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest);
            
            Bookmark bookmark = _bookmarkService.GetBookmark(id.Value);

            if (bookmark == null)
                return new StatusCodeResult(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound);

            return View(bookmark);
        }

        [Authorize]
        public IActionResult Create()
        {
            AddAvailableCategoriesToViewData();
            return View();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Create(BookmarkModel bookmarkModel)
        {
            if (ModelState.IsValid)
            {
                var newBookmark = bookmarkModel.ToBookmark();
                
                if (!string.IsNullOrWhiteSpace(bookmarkModel.CategoryName))
                    newBookmark.Category = _categoryService.EnsureCategoryExists(
                        new Category()
                        { 
                            Name = bookmarkModel.CategoryName 
                        });
                
                _bookmarkService.CreateBookmark(newBookmark);

                return RedirectToAction("Index");
            }

            AddAvailableCategoriesToViewData();
            return View(bookmarkModel);
        }

        [Authorize]
        public IActionResult Edit(int? id)
        {
            if (id == null)
                return new StatusCodeResult(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest);

            Bookmark bookmark = _bookmarkService.GetBookmark(id.Value);

            if (bookmark == null)
                return new StatusCodeResult(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound);

            AddAvailableCategoriesToViewData();
            var bookmarkModel = new BookmarkModel(bookmark);
            
            return View(bookmarkModel);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(BookmarkModel bookmarkModel)
        {
            if (ModelState.IsValid)
            {
                var updateBookmark = bookmarkModel.ToBookmark();

                if (!string.IsNullOrWhiteSpace(bookmarkModel.CategoryName))
                    updateBookmark.Category = _categoryService.EnsureCategoryExists(
                        new Category() 
                        {
                            Name = bookmarkModel.CategoryName 
                        });
                
                _bookmarkService.UpdateBookmark(updateBookmark);
                
                return RedirectToAction("Index");
            }
            
            AddAvailableCategoriesToViewData();
            return View(bookmarkModel);
        }

        [Authorize]
        public IActionResult Delete(int? id)
        {
            if (id == null)
                return new StatusCodeResult(Microsoft.AspNetCore.Http.StatusCodes.Status400BadRequest);
            
            Bookmark bookmark = _bookmarkService.GetBookmark(id.Value);

            if (bookmark == null)
                return new StatusCodeResult(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound);
            
            return View(bookmark);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Bookmark bookmark = _bookmarkService.GetBookmark(id);
            _bookmarkService.DeleteBookmark(bookmark);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize]
        public JsonResult SuggestCategories(string categoryNameStartsWith)
        {
            IEnumerable<string> matched = _categoryService
                .GetCategories()
                .Select(c => c.Name)
                .Where(c => c.ToLower().StartsWith(categoryNameStartsWith));

            return Json(matched);
        }

        private void AddAvailableCategoriesToViewData()
        {
            var availableCategoryNames = _categoryService.GetCategories().Select(c => c.Name);
            ViewData.Add("AvailableCategoryNames", availableCategoryNames);
        }

    }
}
