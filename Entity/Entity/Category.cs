﻿using System.ComponentModel.DataAnnotations;

namespace Entity
{
    public class Category
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string UserID { get; set; }

        [Required]
        [StringLength(maximumLength: 50)]
        public string Name { get; set; }
    }
}
