﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entity
{
    public class Bookmark
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [StringLength(maximumLength: 500)]
        public string URL { get; set; }

        [Required]
        public string ShortDescription { get; set; }

        public int? CategoryId { get; set; }

        [Required]
        public DateTime CreateDate { get; set; }

        [Required]
        public string UserID { get; set; }

        public virtual Category Category { get; set; }
    }
}
